# coding: utf-8
'''
File to compare the affect of compound
interest with different distributions of
mean rate of return.

author: Michael Street
'''
import random
import matplotlib.pyplot as plt
import numpy as np
import os

class AnnualCompoundingInvestment():
    """
    Establishes an annually compounding investment
    type calculation
    """

    def __init__(self, P, t, n, commission, mu, sigma):
        self.P = P # annual investment principal
        self.t = t # number of years to consider
        self.inflation = 0.025
        self.n = n # number of periods of compounding per year
        self.commission = commission # mutual fund annual commission
        self.mu = mu # mean of 30 year plus rate of return
        self.sigma = sigma # standard deviation of annual return data

    def annualReturn(self, r, A):
        A += (self.P - self.P*self.commission)
        return A*(1 + (r-self.inflation)/self.n)**self.n

    def longTermReturn(self):
        A = 0
        for year in range(self.t):
            r = random.gauss(self.mu, self.sigma)
            A = self.annualReturn(r, A)
        return A

    def simulate(self, n):
        outcomes = []
        for scenario in range(n):
            outcomes.append(self.longTermReturn())
        return outcomes

class CompareInvestmentStrategies():
    def __init__(self, strategy1, strategy2, n=2000):
        self.n = n
        self.strategies = [strategy1, strategy2]
        self.results = [strategy.simulate(self.n) for strategy in self.strategies]

    def plot_hist_comparison(self, figureFile, xlim):
        # plot the results
        cm = plt.get_cmap('plasma')
        x = [0.3, 0.7]
        f, (ax1, ax2) = plt.subplots(nrows=2,ncols=1,sharex=True)

        # first strategy
        ax1.hist(np.array(self.results[0])/1e6, 50, normed=0, facecolor=cm(x[0]))
        ax1.set_xlim(xlim)
        ax1.set_ylabel('Frequency')
        ax1.set_title('Cumulative Return Comparison\n t: {} years, $\mu$: {}, $\sigma$: {}, commission: {}'.format(
            self.strategies[0].t, self.strategies[0].mu, self.strategies[0].sigma, self.strategies[0].commission))
        ax1.grid(True)

        # second strategy
        ax2.hist(np.array(self.results[1])/1e6, 50, normed=0, facecolor=cm(x[1]))
        ax2.set_title('t: {} years, $\mu$: {}, $\sigma$: {}, commission: {}'.format(
            self.strategies[1].t, self.strategies[1].mu, self.strategies[1].sigma, self.strategies[1].commission))
        ax2.set_xlabel('Millions (2016)[$]')        
        ax2.set_ylabel('Frequency')
        ax2.grid(True)

        # save results
        plt.savefig(figureFile)

    def get_cumulative_distributions(self):
        self.uniques = [np.unique(x, return_counts=True) for x in self.results]
        self.y = [np.cumsum(i) for i in [item[1] for item in self.uniques]]
        self.x = [np.sort(i) for i in self.results]

    def plot_ecdf(self, figOut, xlim):
        cm = plt.get_cmap('plasma')
        x = [0.3, 0.7]
        f, (ax1) = plt.subplots(nrows=1,ncols=1)
        self.get_cumulative_distributions()
        labels = ['$\mu$:{:.3f}, $\sigma$: {:.3f}, $C$: {:.3f}'.format(strategy.mu,
                                                                       strategy.sigma,
                                                                       strategy.commission)
                  for strategy in self.strategies]
        
                  
        for index, data in enumerate(self.y):
            ax1.step(self.x[index]/1e6, data/np.max(data), color=cm(x[index]), linewidth=1.5,label=labels[index])

        ax1.grid(True)
        ax1.legend(frameon=False, loc=0)
        ax1.set_xlabel('Millions (2016)[$]')
        ax1.set_xlim(xlim)
        ax1.set_ylabel('Probability')
        plt.savefig(figOut)       
        
def main():
    xlim = (0, 4)
    os.chdir('images')
    # case 1: same std. dev., different means
    strategy1 = AnnualCompoundingInvestment(10000,30, 1, 0.0575, 0.095, 0.1677)
    strategy2 = AnnualCompoundingInvestment(10000,30, 1, 0.0,  0.0820, 0.1677)
    comparison = CompareInvestmentStrategies(strategy1, strategy2)
    figOut = 'frequency_plot_comparison_1.png'
    comparison.plot_hist_comparison(figOut, xlim)
    figOut = 'cdf_plot_comparison_1.png'
    comparison.plot_ecdf(figOut, xlim)

    # case 2: same std. dev., same means
    strategy1 = AnnualCompoundingInvestment(10000,30, 1, 0.0575, 0.0820, 0.1677)
    strategy2 = AnnualCompoundingInvestment(10000,30, 1, 0.0,  0.0820, 0.1677)
    comparison = CompareInvestmentStrategies(strategy1, strategy2)
    figOut = 'frequency_plot_comparison_2.png'
    comparison.plot_hist_comparison(figOut, xlim)
    figOut = 'cdf_plot_comparison_2.png'
    comparison.plot_ecdf(figOut, xlim)

    # case 3: managed has better mean, worse std. dev
    strategy1 = AnnualCompoundingInvestment(10000,30, 1, 0.0575, 0.095, 0.2177)
    strategy2 = AnnualCompoundingInvestment(10000,30, 1, 0.0,  0.0820, 0.1677)
    comparison = CompareInvestmentStrategies(strategy1, strategy2)
    figOut = 'frequency_plot_comparison_3.png'
    comparison.plot_hist_comparison(figOut, xlim)
    figOut = 'cdf_plot_comparison_3.png'
    comparison.plot_ecdf(figOut, xlim)

    # case 4: managed has better mean, worse std. dev
    strategy1 = AnnualCompoundingInvestment(10000,30, 1, 0.0575, 0.12, 0.1)
    strategy2 = AnnualCompoundingInvestment(10000,30, 1, 0.0,  0.105, 0.05)
    comparison = CompareInvestmentStrategies(strategy1, strategy2)
    figOut = 'frequency_plot_comparison_4.png'
    comparison.plot_hist_comparison(figOut, xlim)
    figOut = 'cdf_plot_comparison_4.png'
    comparison.plot_ecdf(figOut, xlim)

    # case 5: managed has better mean, same std. dev
    strategy1 = AnnualCompoundingInvestment(10000,30, 1, 0.0575, 0.12, 0.1)
    strategy2 = AnnualCompoundingInvestment(10000,30, 1, 0.0,  0.105, 0.1)
    comparison = CompareInvestmentStrategies(strategy1, strategy2)
    figOut = 'frequency_plot_comparison_5.png'
    comparison.plot_hist_comparison(figOut, xlim)
    figOut = 'cdf_plot_comparison_5.png'
    comparison.plot_ecdf(figOut, xlim)
    os.chdir('..')
    
if __name__ == "__main__":
    main()
